import * as React from 'react';
import Box from '@mui/material/Box';
import WorkImage1 from '../../../assets/images/JourneyPage/cap_2.jpeg'
import WorkImage2 from '../../../assets/images/JourneyPage/cap_3.jpeg'
import WorkImage3 from '../../../assets/images/JourneyPage/cap_6.jpeg'
import WorkImage4 from '../../../assets/images/JourneyPage/cap_1.jpeg'
import WorkImage5 from '../../../assets/images/JourneyPage/cap_2.jpeg'
import WorkImage6 from '../../../assets/images/JourneyPage/cap_5.jpeg'

export default function CustomizedTabs() {
    const showEverything = [
        {
            name: "Month-to-Month Commitment",
            label: "Billing",
            image: WorkImage1,
            desc:"No long-term commitments. 30 day planning with a renewal option included. Built for the modern brand and modern team.",
            className: "image-imageContainer"
        },
        {
            name: "Real-Time Billing",
            label: "Planning",
            image: WorkImage2,
            desc: "Forget Estimates and Proposals. We move quick. We’re results focused and bill in real-time as we map new solutions and strategies.",
            className: "image-imageContainer2"
        },
        {
            name: "Return On Design & Digital(RODD)",
            label: "Road",
            image: WorkImage3,
            desc: "We believe in creating an RODD: where your brand can monetize and become highly marketable from our cross-collective design and digital strategies.",
            className: "image-imageContainer3"
        },
        {
            name: "No Scope Creep",
            label: "Discuss",
            image: WorkImage4,
            desc: "Change happens, we get it. Knowing that, we work with you and not against you. Everything is discussed and agreed so we avoid friction points.",
            className: "image-imageContainer4"
        },
        {
            name: "Ditch The Line",
            label: "Priority",
            image: WorkImage5,
            desc: "You get priority, every day and always. No waiting for others in queue before you - you’re our first priority.",
            className: "image-imageContainer"
        },
        {
            name: "Interested in custom options?",
            label: "Custom",
            image: WorkImage6,
            desc: "Let’s discuss your business needs — contact us.",
            className: "image-imageContainer2"
        },
    ]

    return (
        <Box sx={{ width: '100%' }}>

            <Box>
                
                <div>
                    <div className="image-innerContainer setJourney">

                        {
                            showEverything.map((v, i) => (
                                <div className={v.className}>
                                    <div className="image-setContainer">
                                        <img className="image-container" src={v.image} alt="Work Image" />
                                    </div>
                                    <div className="image-tags">
                                        <div className="tag">{v.label}</div>
                                    </div>
                                    <div className="image-cardtitle"> {v.name} </div>
                                    <div className="image-cardtitle-desc"> {v.desc} </div>
                                </div>
                            ))
                        }

                    </div>
                </div>

                
            </Box>

        </Box>
    );
}
