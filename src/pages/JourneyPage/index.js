import './style.scss'
import RocketFire from '../../assets/videos/rocket-fire.gif'
import JourneyWork from './components/journeyWork'
import JourneyComp from '../../assets/images/JourneyPage/journey/journey-smartphone.png'
import { width } from '@mui/system'

const WorkPage = () => {



	return (
		<div className="work_page">
			{/*  HeroCard */}
			<div className="hero_card">
				<div className="img_container">
					<img src={RocketFire} alt="CoverImg" />
				</div>

				<div className="content container_padding">
					<div className="heading">
						<h1>Journey — marketing support	subscription for brands</h1>
					</div>
				</div>
			</div>

			{/* <div className="work-subcontainer">
				<div className="work-containerText">
					Journey — marketing support	subscription for brands
				</div>
				<div className="backlog-text">subscription</div>

			</div> */}
			
			<div className="journey-sub-container">
				<div className="journey-header-desc">
					With Journey, every month your brand will get hours of work, they will be used for your digital needs that require constant attention.
				</div>
				<div className="journey-header-desc">
					Journey is a great way to regulate digital team and make a contribution to your brand. Pay when you need results.
				</div>
				<div className="journey-button">
					<div className="journey-button-text">Contact about journey</div>
				</div>
			</div>


			<div className="work-subcontainer">
				<div className="work-containerText">
					How Journey works
				</div>
				<div className="backlog-text">Journey</div>
			</div>
			

			<div className="work_navbar container_padding ">

				<JourneyWork />

			</div>

			<div className="work-subcontainer">
				<div className="work-containerText">
					Built for
				</div>
				<div className="backlog-text">Built for</div>
			</div>

			<div className="journey-subcontainer setjourney">
				
				<div className="journey-subcontainer1">
					<img className="journey-image" src={JourneyComp} alt="Journey Image" />
				</div>

				<div className="journey-subcontainer2">
					<div className="journey-container1">
						<div className="journey-container-header">Brands</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">Funded Startups</div>
						</div>
						
						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">B2B & B2C Brands</div>
						</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">eCommerce Companies</div>
						</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">Growth-Focused	Organizations</div>
						</div>

					</div>

					<div className="journey-container2">
						<div className="journey-container-header">Teams</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">Funded Startups</div>
						</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">B2B & B2C Brands</div>
						</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">eCommerce Companies</div>
						</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet"></div>
							<div className="bullet-text">Growth-Focused	Organizations</div>
						</div>

					</div>
				</div>
			</div>

			<div className="work-subcontainer">
				<div className="work-containerText">
					Services you get
				</div>
				<div className="backlog-text">Services you get</div>
			</div>

			<div className="service-maincontainer">

				<div className="innerservice-lists">
					<div  className="slider-list">
						<div className="list-heading">Branding & Identity</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Brand Strategy & Experience</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Trends & Insights</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Go-To-Market Strategy</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Guidelines & Systems</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Content Strategy</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Identity Design</div>
						</div>
					</div>
				</div>

				<div className="innerservice-lists">
					<div className="slider-list">
						<div className="list-heading">Websites Platforms</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Digital Strategy</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">UX & UI Design</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Functional Prototyping</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Digital Activation</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Web & App Development</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">SEO Strategy & Systems</div>
						</div>
					</div>
				</div>

				<div className="innerservice-lists">
					<div  className="slider-list">
						<div className="list-heading">eCommerce Experiences</div>

						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Digital Strategy</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Industry & Consumer Research</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Rapid Prototyping</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Design Direction</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">eCommerce Platforms</div>
						</div>
						<div className="bullet-innerflex setBullet">
							<div className="bullet set-bullet"></div>
							<div className="bullet-settext">Digital Guidelines</div>
						</div>
					</div>
				</div>
				

			</div>
		</div>
	)
}

export default WorkPage
