import './style.scss'
import HeroImg from '../../assets/images/CapabilitiesPage/HeroCardBackground.png'
import CoverImage1 from '../../assets/images/CapabilitiesPage/capabilities-img-one.png'
import CoverImage2 from '../../assets/images/CapabilitiesPage/capabilities-img-two.png'
import CoverImage3 from '../../assets/images/CapabilitiesPage/capabilities-img-three.png'
import CoverImage4 from '../../assets/images/CapabilitiesPage/capabilities-img-four.png'
import CoverImage5 from '../../assets/images/CapabilitiesPage/capabilities-img-five.png'

const WorkPage = () => {

	const capabilities = [
		{
			heading: "Branding & Identity",
			desc: "We fuel the growth of purpose driven brands through strategy activation, design empowerment, and market adoption. From cultivating new ideas to connecting the dots for customers or users, these are our core principles.",
			image: CoverImage1,
			className: 'capable-innercontainer',
			points: [
				'Brand Strategy & Experience',
				'Guidelines & Systems',
				'Trends & Insights',
				'Content Strategy',
				'Go-To-Market Strategy',
				'Identity Design'
			]

		},
		{
			heading: "Websites & Digital Platforms",
			desc: "We design digital platforms to empower users and your brand's tribe. This deep understanding of what motivates them allows us to forge and fine-tune the most powerful strategies that generate rapid ROI for your business.",
			image: CoverImage2,
			className: 'capable-inner-reverse-container',
			points: [
				'Digital Strategy',
				'Digital Activation',
				'UX & UI Design',
				'Web & App Development',
				'Functional Prototyping',
				'SEO Strategy & Systems'
			]

		},
		{
			heading: "Ecommerce Experiences",
			desc: "eCommerce is all about experience, and we craft and co-create experiences that are both purposeful and equally profitable. Digital brand building through eCommerce channels fuels business growth and the bottom-line.",
			image: CoverImage3,
			className: 'capable-innercontainer',
			points: [
				'Digital Strategy',
				'Design Direction',
				'Industry & Consumer Research',
				'eCommerce Platforms',
				'Rapid Prototyping',
				'Digital Guidelines'
			]

		},
		{
			heading: "Performance Marketing",
			desc: "With a keen understanding of what's happening in the digital landscape, we leverage the power of marketing platforms to connect audiences with contagious content worth sharing & spreading, cross-channel and touchpoint.",
			image: CoverImage4,
			className: 'capable-inner-reverse-container',
			points: [
				'Growth Strategy',
				'Social Media Marketing',
				'PPC Campaigns',
				'Launch Strategies',
				'Email Marketing',
				'SEO Enablement'
			]

		},
		{
			heading: "VR & AR Environments",
			desc: "Reality is a new reality with virtual reality (VR) and augmented reality (AR) being at the forefront of technology adoption. With a well-built digital strategy we leverage this stellar tech to propel your brand forward.",
			image: CoverImage5,
			className: 'capable-innercontainer',
			points: [
				'Games & Apps',
				'Mixed Reality',
				'Mobile Apps',
				'VR Streaming',
				'VR Platform Design',
				'VR Platform Development'
			]

		},
	]


	return (
		<div className="work_page">
			{/*  HeroCard */}
			<div className="hero_card">
				<div className="img_container">
					<img src={HeroImg} alt="CoverImg" />
				</div>

				<div className="content container_padding">
					<div className="heading">
						<h1>Design-powered to fuel your growth goals</h1>
					</div>
				</div>
			</div>

			<div className="work-subcontainer">
				<div className="work-containerText">
					Our clients share our approach to work
				</div>
				<div className="backlog-text">Our clients share </div>
			</div>

			{/* WorkPage Navabr */}

			<div className="capable-container">

				{
					capabilities.map((v, i) => (
						<div className={v.className}>
							<div className="image-container setd-container">
								<div className="image-innercontainer">
									<div className="image-header">{v.heading}</div>
									<div className="image-desc">
										{v.desc}
									</div>

									<div className="bullet-flex">

										{
											v.points.map((v, i) => (
												<div className="bullet-innerflex">
													<div className="bullet"></div>
													<div className="bullet-text">{v}</div>
												</div>
											))
										}


									</div>

								</div>
							</div>
							<div className="image-container">
								<img className="setImage-container" src={v.image} alt="Cover Image" />
							</div>
						</div>
					))
				}



			</div>

			{/* Content */}

		</div>
	)
}

export default WorkPage
