import * as React from "react";
import { styled } from "@mui/material/styles";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import WorkImage1 from "../../../assets/images/WorkPage/work/WorkImg01.png";
import WorkImage2 from "../../../assets/images/WorkPage/work/WorkImg02.png";
import WorkImage3 from "../../../assets/images/WorkPage/work/WorkImg03.png";
import WorkImage4 from "../../../assets/images/WorkPage/work/WorkImg04.png";
import WorkImage5 from "../../../assets/images/WorkPage/work/WorkImg05.png";
import WorkImage6 from "../../../assets/images/WorkPage/work/WorkImg06.png";
import WorkImage7 from "../../../assets/images/WorkPage/work/WorkImg07.png";
import WorkImage8 from "../../../assets/images/WorkPage/work/WorkImg08.png";
import WorkImage9 from "../../../assets/images/WorkPage/work/WorkImg09.png";
import WorkImage10 from "../../../assets/images/WorkPage/work/WorkImg10.png";

const AntTabs = styled(Tabs)({
  borderBottom: "1px solid #e8e8e8",
  "& .MuiTabs-indicator": {
    backgroundColor: "#1890ff",
  },
});

const AntTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: "none",
    minWidth: 0,
    [theme.breakpoints.up("sm")]: {
      minWidth: 0,
    },
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(1),
    color: "rgba(0, 0, 0, 0.85)",
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:hover": {
      color: "#40a9ff",
      opacity: 1,
    },
    "&.Mui-selected": {
      color: "#1890ff",
      fontWeight: theme.typography.fontWeightMedium,
    },
    "&.Mui-focusVisible": {
      backgroundColor: "#d1eaff",
    },
  })
);

const StyledTabs = styled((props) => (
  <Tabs
    {...props}
    TabIndicatorProps={{ children: <span className="MuiTabs-indicatorSpan" /> }}
  />
))({
  "& .MuiTabs-indicator": {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "transparent",
  },
  "& .MuiTabs-indicatorSpan": {
    maxWidth: 40,
    width: "100%",
    backgroundColor: "#635ee7",
  },
});

const StyledTab = styled((props) => <Tab disableRipple {...props} />)(
  ({ theme }) => ({
    textTransform: "none",
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: theme.typography.pxToRem(15),
    marginRight: theme.spacing(1),
    color: "rgba(255, 255, 255, 0.7)",
    "&.Mui-selected": {
      color: "#0088E2",
    },
    "&.Mui-focusVisible": {
      backgroundColor: "rgba(100, 95, 228, 0.32)",
    },
  })
);

export default function CustomizedTabs() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const showEverything = [
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage1,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage2,
      className: "image-imageContainer2",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage3,
      className: "image-imageContainer3",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage4,
      className: "image-imageContainer4",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage5,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage6,
      className: "image-imageContainer2",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage7,
      className: "image-imageContainer3",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage8,
      className: "image-imageContainer4",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage9,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage10,
      className: "image-imageContainer2",
    },
  ];

  const brandingIdentity = [
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage8,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage9,
      className: "image-imageContainer2",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage10,
      className: "image-imageContainer3",
    },
  ];

  const digitalPlatform = [
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage5,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage6,
      className: "image-imageContainer2",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage7,
      className: "image-imageContainer3",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage8,
      className: "image-imageContainer4",
    },
  ];

  const ecommerceExperience = [
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage1,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage3,
      className: "image-imageContainer2",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage4,
      className: "image-imageContainer3",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage6,
      className: "image-imageContainer4",
    },
  ];

  const performanceMarketing = [
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage4,
      className: "image-imageContainer",
    },
    {
      name: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      label: "Performance Marketing",
      image: WorkImage6,
      className: "image-imageContainer2",
    },
  ];

  return (
    <Box sx={{ width: "100%" }}>
      <Box>
        <div className="setStyledTabs">
          <StyledTabs
            style={{
              display: "flex",
              justifyContent: "center",
              width:1000
            }}
            value={value}
            onChange={handleChange}
            aria-label="styled tabs example"
          >
            <StyledTab className="image-tabs" label="Show everything" />
            <StyledTab className="image-tabs" label="Branding & Identity" />
            <StyledTab
              className="image-tabs"
              label="Websites & Digital Platforms"
            />
            <StyledTab className="image-tabs" label="eCommerce Experiences" />
            <StyledTab className="image-tabs" label="Performance Marketing" />
          </StyledTabs>

        </div>
        <div>
          {value == 0 ? (
            <div className="image-innerContainer">
              {showEverything.map((v, i) => (
                <div className={v.className}>
                  <div className="image-setContainer">
                    <img
                      className="image-container image-setcontainer"
                      src={v.image}
                      alt="Work Image"
                    />
                  </div>
                  <div className="image-tags">
                    <div className="tag">{v.label}</div>
                  </div>
                  <div className="image-cardtitle"> {v.name} </div>
                </div>
              ))}
            </div>
          ) : null}

          {value == 1 ? (
            <div className="image-innerContainer">
              {brandingIdentity.map((v, i) => (
                <div className={v.className}>
                  <div className="image-setContainer">
                    <img
                      className="image-container"
                      src={v.image}
                      alt="Work Image"
                    />
                  </div>
                  <div className="image-tags">
                    <div className="tag">{v.label}</div>
                  </div>
                  <div className="image-cardtitle"> {v.name} </div>
                </div>
              ))}
            </div>
          ) : null}

          {value == 2 ? (
            <div className="image-innerContainer">
              {digitalPlatform.map((v, i) => (
                <div className={v.className}>
                  <div className="image-setContainer">
                    <img
                      className="image-container"
                      src={v.image}
                      alt="Work Image"
                    />
                  </div>
                  <div className="image-tags">
                    <div className="tag">{v.label}</div>
                  </div>
                  <div className="image-cardtitle"> {v.name} </div>
                </div>
              ))}
            </div>
          ) : null}

          {value == 3 ? (
            <div className="image-innerContainer">
              {ecommerceExperience.map((v, i) => (
                <div className={v.className}>
                  <div className="image-setContainer">
                    <img
                      className="image-container"
                      src={v.image}
                      alt="Work Image"
                    />
                  </div>
                  <div className="image-tags">
                    <div className="tag">{v.label}</div>
                  </div>
                  <div className="image-cardtitle"> {v.name} </div>
                </div>
              ))}
            </div>
          ) : null}

          {value == 4 ? (
            <div className="image-innerContainer">
              {performanceMarketing.map((v, i) => (
                <div className={v.className}>
                  <div className="image-setContainer">
                    <img
                      className="image-container"
                      src={v.image}
                      alt="Work Image"
                    />
                  </div>
                  <div className="image-tags">
                    <div className="tag">{v.label}</div>
                  </div>
                  <div className="image-cardtitle"> {v.name} </div>
                </div>
              ))}
            </div>
          ) : null}
        </div>
      </Box>
    </Box>
  );
}
