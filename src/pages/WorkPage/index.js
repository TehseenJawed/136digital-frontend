import './style.scss'

import HeroImg from '../../assets/images/WorkPage/CoverImg.png'
import ImageTabs from './components/imageTabs'

const WorkPage = () => {
	return (
		<div className="work_page">
			{/*  HeroCard */}
			<div className="hero_card">
				<div className="img_container">
					<img src={HeroImg} alt="CoverImg" />
				</div>

				<div className="content container_padding">
					<div className="heading">
						<h1>Work</h1>
					</div>
				</div>
			</div>

			<div className="work-subcontainer">
				<div className="work-containerText">
					Helping brands to create experiences that's people love
				</div>
				<div className="backlog-text">Helping brands to </div>
			</div>

			{/* WorkPage Navabr */}
			<div className="work_navbar container_padding ">
				{/* <ul>
					<li className="active">Show everything</li>
					<li>Branding & Identity</li>
					<li>Websites & Digital Platforms</li>
					<li>eCommerce Experiences</li>
					<li>Performance Marketing</li>
				</ul> */}
			<ImageTabs />
			</div>

			{/* Content */}
			
		</div>
	)
}

export default WorkPage
