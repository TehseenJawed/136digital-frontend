import "./style.scss";
import HeroImg from "../../assets/images/AboutusPage/HeroImg.png";
import CapabilitiesImg from "../../assets/images/AboutusPage/1.png";
import { useState, useRef } from "react";
import { CgArrowLongRight } from "react-icons/cg";
import { BsCircleFill, BsPlayFill } from "react-icons/bs";
import { BiRightArrowAlt } from "react-icons/bi";
import { Link } from "react-router-dom";

// 3D Images
// import Img1 from "../../assets/images/AboutusPage/3DImages/1.png";
// import Img2 from "../../assets/images/AboutusPage/3DImages/2.png";
// import Img3 from "../../assets/images/AboutusPage/3DImages/3.png";
// import Img4 from "../../assets/images/AboutusPage/3DImages/4.png";
// import Img5 from "../../assets/images/AboutusPage/3DImages/5.png";

import MissionImg from "../../assets/images/AboutusPage/MissionImg.png";

import BrandStory1 from "../../assets/images/AboutusPage/BrandStory1.png";
import BrandStory2 from "../../assets/images/AboutusPage/BrandStory2.png";
import BrandStory3 from "../../assets/images/AboutusPage/BrandStory3.png";
import BrandSection from "./../../components/BrandSection/index";

// Brand Cooperation Images Import
import FacebookImg from "../../assets/images/AboutusPage/FacebookImg.png";
import GoogleImg from "../../assets/images/AboutusPage/GoogleImg.png";
import HubSpotImg from "../../assets/images/AboutusPage/HubSpotImg.png";
import MailchimpImg from "../../assets/images/AboutusPage/MailchimpImg.png";
import SnapshotImg from "../../assets/images/AboutusPage/FacebookImg.png";
import YoutubeImg from "../../assets/images/AboutusPage/YoutubeImg.png";
import RightCarusal from "./../../components/RightCarusal/index";
// Brand Cooperation Images Import
import Imagee1 from "../../assets/images/mainPage/brand/1.svg";
import Imagee2 from "../../assets/images/mainPage/brand/2.svg";
import Imagee3 from "../../assets/images/mainPage/brand/3.svg";
import Imagee4 from "../../assets/images/mainPage/brand/4.svg";
import Imagee5 from "../../assets/images/mainPage/brand/5.svg";
import Imagee6 from "../../assets/images/mainPage/brand/6.svg";

//Video
import Vid1 from "../../assets/videos/main-page/vid1.mp4";

import News1 from "../../assets/images/AboutusPage/News1.png";
import News2 from "../../assets/images/AboutusPage/News2.png";
import News3 from "../../assets/images/AboutusPage/News3.png";
import News4 from "../../assets/images/AboutusPage/News4.png";
import News5 from "../../assets/images/AboutusPage/News5.png";
import News6 from "../../assets/images/AboutusPage/News6.png";

const AboutUsPage = () => {
  const [videoPlay, setVideoPlay] = useState(false);
  const videoRef = useRef(null);

  const NewsLinkdinContentArray = [
    {
      id: 1,
      img: News1,
      date: "March 10, 2021",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 2,
      img: News2,
      date: "March 10, 2021",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 3,
      img: News3,
      date: "March 10, 2021",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 4,
      img: News4,
      date: "March 10, 2021",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
  ];

  const NewsMediumContentArray = [
    {
      id: 1,
      img: News1,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 2,
      img: News2,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 3,
      img: News3,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 4,
      img: News4,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 5,
      img: News5,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 6,
      img: News6,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
  ];

  const NewsInstagramContentArray = [
    {
      id: 1,
      img: News4,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 2,
      img: News5,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 3,
      img: News6,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 4,
      img: News1,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 5,
      img: News2,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
    {
      id: 6,
      img: News3,
      date: "March 10, 2021 ",
      heading: "136 DIGITAL. Company news headline",
      description:
        "Brief description of the article. Perhaps 2-3 lines. No more than that.",
    },
  ];

  const handleClick = (event) => {
    videoRef.current.play();
    setVideoPlay(true);
  };

  return (
    <div className="aboutus_page">
      <div className="hero_card">
        <div className="hero_card_img">
          <img src={HeroImg} alt="Hero" />
        </div>

        <div className="content">
          <h1> We drive experiences for brands with purpose. </h1>
        </div>
      </div>

      <div className="capabilities_card">
        <div className="content-body">
          <div className="img_container">
            <img src={CapabilitiesImg} alt="" />
          </div>
          <div className="content-container col-lg-6 col-12">
            <div className="content">
              <h2>Work that matters for people who care</h2>
              <p>
                136 Digital is a Full-service Branding and Marketing agency that
                helps businesses create lead generation and relationships with
                targeted audiences through powerful emotionally-driven
                storytelling.
              </p>
              <p>
                Working closely with Brands to help establish themselves in the
                digital marketplace and differentiate themselves from their
                competition with tailored narrative-driven branding that shows
                through the way they present every aspect of their business.
              </p>

              <div className="set-button">
                <div className="work_btn_div">
                  <Link style={{ textDecoration: 'none' }}  to="/capabilities">
                    <div className="buttonContainer">
                      <div className="footer-button">
                        <div className="foolter-icon-button">
                          <BiRightArrowAlt color={"white"} size={30} />
                        </div>
                        <div className="foolter-icon-card">Our capability</div>
                      </div>
                      <div className="foolter-icon-card-absolute">
                        Our capability
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="lg_content">
          <div className="heading-container">
            <h1>Carter Malone</h1>
            <h1>Home of the 136digital detectives</h1>
          </div>
        </div>
        {/* <div className="images">
          <img src={Img1} />
          <img src={Img2} />
          <img src={Img3} />
          <img src={Img4} />
          <img src={Img5} />
        </div> */}
        <div className="video_button">
          <video
            ref={videoRef}
            onPlaying={() => {
              setVideoPlay(true);
              console.log("video playing");
            }}
            onPause={() => {
              setVideoPlay(false);
              console.log("video paused");
            }}
            onEnded={() => {
              setVideoPlay(false);
              console.log("video ended");
            }}
            controls
          >
            <source src={Vid1} type="video/mp4" />
          </video>
          {videoPlay == false ? (
            <div
              onClick={(e) => {
                console.log("wow");
                handleClick(e);
              }}
              className="playButton"
            >
              <div className="outerCircle">
                <div className="midCircle">
                  <div className="innerCircle">
                    <BsPlayFill style={{ fontSize: "50px", color: "white" }} />
                  </div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>

      {/* Mission Section */}
      <div className="mission_section">
        <div className="img_container">
          <img src={MissionImg} alt="" />
        </div>

        <div className="content">
          <div className="content-detail">
            <h1>Mission</h1>
            <p>
              Our Mission is to take brands on a journey providing work that
              matters for people who care through the art of digital story
              telling helping businesses tell their unique stories in a way that
              connects and relates to communities.
            </p>
          </div>
        </div>
      </div>

      {/* Brand Copration */}
      <div className="brand_story_section">
        <div className="heading">
          <h1>Brand Story</h1>
        </div>

        <div className="brand-story-mainContainer">
          <div className="container-1">
            <div className="image-brand-story">
              <img src={BrandStory2} alt="" />
            </div>
            <div className="content-brand-story">
              <p className="para1">
                While growing up in Southeast London and communicating between two
                rival gangs, I discovered my own definition and style of branding.
              </p>
              <p className="para1">
                I quickly realized that storytelling was my most powerful tool; wherever I
                was, every day was colorful, and communicating with those around me was
                vital.
              </p>
              <p className="para1">
                Most of these stories would occur on the #136 bus while it chugged
                through all the gangs’ hotspots. These hotspots hosted colorful personalities
                and lifestyles. By the time you arrived at your own stop and walked down
                the aisle to exit the bus, you always had a story to tell.
              </p>
              <p className="para1">
                Personal branding flourished in these areas; the way you carried yourself
                told others who you were, what you did, who you knew, and what you’d
                seen.
              </p>
            </div>
          </div>
          <div className="container-2">
            <div className="content-brand-story">
              <h2>New: Ideas come from challenges</h2>
              <p>We are here to tell the stories of people and brands.</p>
              <p>
                Over the last ten years, we’ve championed people and brands to place them
                in advantageous positions in their markets. We’ve found new ways to
                engage with brands to turn our own client relationships into partnerships
                that create groundbreaking turnover for both parties. Through honesty and
                integrity, we make a difference for people that care.
              </p>
              <h5>New:</h5>

              <div className="list-item">
                <div className="bullet-innerflex2">
                  <div className="bullet"></div>
                  <div className="about-bullet-text">
                    The customer’s experience is the base of everything we do
                  </div>
                </div>
              </div>

              <div className="list-item">
                <div className="bullet-innerflex2">
                  <div className="bullet"></div>
                  <div className="about-bullet-text">
                    Relationships are built on trust that is developed through consistency
                  </div>
                </div>
              </div>

              <div className="list-item">
                <div className="bullet-innerflex2">
                  <div className="bullet"></div>
                  <div className="about-bullet-text">
                    Let us take your brand on a digital journey to reach your audience
                  </div>
                </div>
              </div>

            </div>
            <div className="image-brand-story">
              <img src={BrandStory1} alt="" />
            </div>
          </div>
          <div className="container-1">
            <div className="image-brand-story">
              <img src={BrandStory3} alt="" />
            </div>
            <div className="content-brand-story">
              <div className="content-list">
                <div className="list-item">
                  <div className="bullet-innerflex2">
                    <div className="bullet"></div>
                    <div className="about-bullet-text">
                      The Customers experience is the base of everything we do.
                    </div>
                  </div>
                </div>
                <div className="list-item">
                  <div className="bullet-innerflex2">
                    <div className="bullet"></div>
                    <div className="about-bullet-text">
                      Relationships are built on trust, which is developed through consistently and promise.
                    </div>
                  </div>
                </div>
                <div className="list-item">
                  <div className="bullet-innerflex2">
                    <div className="bullet"></div>
                    <div className="about-bullet-text">
                      Let us take your brand on a digital journey to reach your audience.
                   </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Brand Cooperation */}
      <BrandSection
        Image1={Imagee1}
        Image2={Imagee2}
        Image3={Imagee3}
        Image4={Imagee4}
        Image5={Imagee5}
        Image6={Imagee6}
      />

      {/* News from Medium */}
      <RightCarusal
        title="News from Medium"
        buttonText="Our Medium"
        ContentArray={NewsMediumContentArray}
      />

      {/* News From Instagram */}
      <RightCarusal
        title="News from Instagram"
        buttonText="Our Instagram"
        ContentArray={NewsInstagramContentArray}
      />

      {/* News from Linkdin */}
      <RightCarusal
        title="News from LinkedIn"
        buttonText="Our Linkdin"
        ContentArray={NewsLinkdinContentArray}
      />
    </div>
  );
};

export default AboutUsPage;
