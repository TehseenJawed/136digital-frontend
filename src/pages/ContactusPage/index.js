
import MessageIcon from "../../assets/images/ContactUsPage/message-icon.png";
import CallIcon from "../../assets/images/ContactUsPage/call-icon.png";
import LocationIcon from "../../assets/images/ContactUsPage/location-sharp.png";
import React, { useState } from "react";
import "./style.scss";
import RocketFire from "../../assets/videos/rocket-fire.gif";
import JourneyComp from "../../assets/images/contactus-cover.png";
import axios from "axios";

const WorkPage = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [textField, setTextField] = useState("");

  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      const newObj = {
        email: "cloudsoftpros@gmail.com",
        password: "!551892Rofi",
        recipents: [email, 'Demetreous89@gmail.com'],
        subject: "136 Digital: Message has received.",
        message: `<html>
          <br /><p>Hello ${name}. This is to clearify that we have received your message and one of our representative will be in touch of you.</p>
          <br /><br /><b>Your Message.</b>
          <br /><p>${textField}</p>
          <br /><p>Regards,</p>
          <br /><h5> 136 Digital</h5>
        </html>`,
      };
      console.log("Its status ---->>>  2");
      const response = await axios.post('https://dipixels-mail-services.herokuapp.com/api/mail', newObj)
      console.log("Its status ---->>>  ", response.status);
    } catch (err) {
      console.log(err.response.data.message)
    }
  }

  return (
    <div className="work_page">
      <div className="hero_card">
        <div className="img_container">
          <img src={JourneyComp} alt="CoverImg" />
        </div>
        <div className="content container_padding">
          <div className="heading">
            <h1>Contact</h1>
          </div>
        </div>
      </div>

      <div className="work-subcontainer">
        <div className="work-containerText">Get in touch</div>
        <div className="backlog-text">Get in touch</div>
      </div>

      <div className="journey-sub-container">
        <div className="journey-header-desc">
          Feel free to contact us. We will try to answer all your questions.
        </div>
      </div>

      <div className="work_navbar container_padding ">
        <form onSubmit={handleSubmit} className="contactus-form">
          <div className="form-divider">
            <div className="field-container">
              <div className="field-label">
                Your Name <span style={{ color: "#FF0000" }}>*</span>
              </div>
              <input
                className="contact-field"
                name={name}
                onChange={(e) => setName(e.target.value)}
                type="text"
                placeholder="Name here"
                required
              />
            </div>
            <div className="field-container">
              <div className="field-label">
                Your Email <span style={{ color: "#FF0000" }}>*</span>
              </div>
              <input
                className="contact-field"
                name={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                placeholder="Email here"
                required
              />
            </div>
          </div>
          <div className="form-messagedivider">
            <div className="field-label">
              Message <span style={{ color: "#FF0000" }}>*</span>
            </div>
            <textarea
              className="contact-textfield"
              placeholder="Your message here..."
              value={textField}
              onChange={(e) => setTextField(e.target.value)}
              name="w3review"
              rows="6"
              cols="50"
              required
            />
          </div>
          <div>
            <input className="contactus-button" type="submit" value="Send" />
          </div>
        </form>
        <div className="contactUs-icon-section">
          <div className="contactUs-icons">
            <div className="message-us-box">
              <img className="contact-icon" src={MessageIcon}></img>
              <p>Discovery@136digital.com</p>
            </div>
            <hr className="vertical-line" width="1" size="200"></hr>
            <div className="message-us-box">
              <img className="contact-icon" src={CallIcon}></img>
              <p>
                <a
                  style={{ textDecoration: "none", color: "#848484" }}
                  href="tel:02031433149"
                >
                  02031433149
                </a>
              </p>
            </div>
            <hr className="vertical-line" width="1" size="200"></hr>
            <div className="message-us-box">
              <img className="contact-icon" src={LocationIcon}></img>
              <p>
                1st Floor 2 wood berry Avenue, North Finchley, London, N12 0lr
              </p>
            </div>
          </div>
        </div>
        <div id="googleMap" style={{ width: "100%", height: "400px" }}>
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d32661597.019252103!2d5.152916311796914!3d2.1493397417536078!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x10a06c0a948cf5d5%3A0x108270c99e90f0b3!2sAfrica!5e0!3m2!1sen!2s!4v1639487828872!5m2!1sen!2s"
            loading="lazy"
            style={{ width: "100%", height: "550px" }}
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default WorkPage;
