import React, { useState } from "react";
import './FloatForm.scss';
import { VscDebugStepBack } from 'react-icons/vsc'
import CardImage from '../../assets/images/cardIllustration.png'
import Video from '../../assets/videos/form_video.mp4';
import ReactPlayer from 'react-player'

function FloatForm() {
    const [buttonAnimation, setButtonAnimation] = useState(true);
    const [visible, setVisibal] = useState(true);
    console.log(buttonAnimation);

    return visible ? (
        <div className="float-form">
            <div className="innerfloat-1">
                <VscDebugStepBack style={{cursor:'pointer'}} onClick={() => setVisibal(!visible)} size={25} color="gray" />


                <div className="form-header">
                    136 digital detectives
                </div>
                <div className="form-desc">
                    Carter and Malone on hand to help .let’s discuss
                </div>
                <div className="form-side-button">
                    Discuss Project
                </div>

            </div>
            <div className="innerfloat-2">
                <ReactPlayer height={230} 
                    className="card-image" 
                    url={Video} 
                    playing
                    volume={0}
                    muted={true}/>
            </div>

        </div>
    ) : (
        <div></div>
    )
}

export default FloatForm
