import "./style.scss";
import { useState, useEffect } from "react";

import FooterImage from "../../assets/images/footerImage.png";
// import { CgArrowLongRight } from "react-icons/cg";
import NewFooterImage from "../../assets/images/mainPage/backgroundimg1.png";
import NewFooterImage2wow from "../../assets/images/mainPage/footerBgLast.png";
import FooterLogo from "../../assets/images/header-logo-white.svg";
import { Link, useLocation } from "react-router-dom";
import {
  TiSocialFacebook,
  TiSocialTwitter,
  TiSocialInstagram,
} from "react-icons/ti";
import { SiMedium } from "react-icons/si/";
import { GiHamburgerMenu } from "react-icons/gi";
import { CgArrowLongRight } from "react-icons/cg";
import { RiArrowUpSLine } from "react-icons/ri";
import { BiRightArrowAlt } from "react-icons/bi";

const Footer = () => {
  const { pathname } = useLocation();
  const [headerColor, setHeaderColor] = useState("black");

  useEffect(() => {
    if (pathname === "/") {
      setHeaderColor("white");
    } else {
      setHeaderColor("black");
    }
  }, [pathname]);

  return (
    <div className="footer">
      <div className="footer_content_row">
        <img src={NewFooterImage} alt="footerimage" />

        <div className="content">
          <div className="slide-container">
            <h1>Let's cooperate</h1>
            <p>
              We are always on hand to help brands communicate effectively with
              their targeted audiences. Drop us a line about your project. Our
              team will contact you shortly
            </p>

            <div className="buttonContainer">
              <div className="footer-button">
                <div className="foolter-icon-button">
                  <BiRightArrowAlt color={"#00253F"} size={30} />
                </div>
                <div className="foolter-icon-card">Discuss Project</div>
              </div>
              <div className="foolter-icon-card-absolute">Discuss Project</div>
            </div>
            {/* <button>
              Discuss project <CgArrowLongRight className="arrow" />
            </button> */}
          </div>
        </div>
      </div>
      <div className="blackImage">
        <img style={{ width: "100%" }} src={NewFooterImage2wow}></img>
      </div>
      <div className="Testing-parent">
      <div className="header_links">
        <div className="page-links ">
            <Link
              className={`footer_link ${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              to="/about-us"
            >
              About us
            </Link>
            {/* <Link
              className={`footer_link ${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              to="/work"
            >
              Work
            </Link> */}
            <Link
              className={`footer_link ${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              to="/capabilities"
            >
              Capabilities
            </Link>
            <Link
              className={`footer_link ${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              to="/journey"
            >
              Journey
            </Link>
            <Link
              className={`footer_link ${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              to="/contact-us"
            >
              Contact
            </Link>
          </div>
        </div>

        <div className="setLinks">
          <div className="copyright">
            © 2021 – 136 Digital | All Rights Reserved
          </div>
          
          <div className="socialmedia-icon">
            <a className="social-link" href="https://www.facebook.com/">
              <TiSocialFacebook />
            </a>
            <a className="social-link" href="https://twitter.com/">
              <TiSocialTwitter />
            </a>
            <a className="social-link" href="https://www.instagram.com/">
              <TiSocialInstagram />
            </a>
            <a className="social-link" href="https://www.medium.com/">
              <SiMedium />
            </a>
          
        </div>
        

        <div className="blackBox">
          <RiArrowUpSLine style={{ color: "white" }} />
        </div>
      </div>
      </div>
      
    </div>
  );
};

export default Footer;
