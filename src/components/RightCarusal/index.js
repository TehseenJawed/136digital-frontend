import "./style.scss";
import { useState } from "react";
import ArrowNext from "../../assets/images/next-icon.svg";
import ArrowPrevious from "../../assets/images/previous-icon.svg";
import { MdOutlineDateRange } from "react-icons/md";
import cupImg from "../../assets/images/mainPage/News/cup.png";
import { useRef } from "react";
import { Link } from "react-router-dom";

import { CgArrowLongRight } from "react-icons/cg";

{
  /* <RightCarusal
title="News from Medium"
buttonText="Our Medium"
ContentArray={NewsMediumContentArray}
/> */
}

const RightCarusal = ({ ContentArray, title, buttonText }) => {
  const [alterImage, setAlterImage] = useState(true);
  const [alterImage1, setAlterImage1] = useState(true);
  const [alterImage2, setAlterImage2] = useState(true);
  //   const [title, setTitle] = useState(title);

  console.log(title, "<=== title");
  console.log(ContentArray, "<=== content");
  const scrollRef = useRef(null);
  const NewsArray = ContentArray;
  const scrollLeft = () => {
    if (scrollRef.current) {
      scrollRef.current.scrollBy({
        top: 0,
        left: -400,
        behavior: "smooth",
      });
    }
  };

  const scrollRight = () => {
    if (scrollRef.current) {
      scrollRef.current.scrollBy({
        top: 0,
        left: 480,
        behavior: "smooth",
      });
    }
  };

  return (
    <div className="carousel-maincontainer">
      <div className="heading">
        {title == "News from Medium" ? (
          <div className="title">
            <h1 className="title-is-medium">{title}</h1>
          </div>
        ) : null}
        {title == "News from Instagram" ? (
          <div className="title">
            <h1 className="title-is-instagram">{title}</h1>
          </div>
        ) : null}
        {title == "News from LinkedIn" ? (
          <div className="title">
            <h1 className="title-is-linkedin">{title}</h1>
          </div>
        ) : null}
      </div>

      <div className="news_container">
        <div
          onMouseEnter={() => setAlterImage(false)}
          onMouseLeave={() => setAlterImage(true)}
          id="work_card0"
          className="work_card"
          key={NewsArray[0].id}
        >
          <div className="image-container">
            {alterImage ? (
              <img src={NewsArray[0].img} alt="work" />
            ) : (
              <img src={cupImg} alt="work" />
            )}
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[0].date}
            </span>
          </div>

          <div>
            <h2 className="carousel-text">{NewsArray[0].heading}</h2>
          </div>
        </div>
        <div
          onMouseEnter={() => setAlterImage1(false)}
          onMouseLeave={() => setAlterImage1(true)}
          id="work_card1"
          className="work_card"
          key={NewsArray[1].id}
        >
          <div className="image-container">
            {alterImage1 ? (
              <img src={NewsArray[1].img} alt="work" />
            ) : (
              <img src={cupImg} alt="work" />
            )}
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[1].date}
            </span>
          </div>

          <div style={{ marginTop: "10px" }}>
            <h2 className="carousel-text">{NewsArray[1].heading}</h2>
          </div>
        </div>
        <div
          onMouseEnter={() => setAlterImage2(false)}
          onMouseLeave={() => setAlterImage2(true)}
          id="work_card2"
          className="work_card"
          key={NewsArray[2].id}
        >
          <div className="image-container">
            {alterImage2 ? (
              <img src={NewsArray[2].img} alt="work" />
            ) : (
              <img src={cupImg} alt="work" />
            )}
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[2].date}
            </span>
          </div>

          <div style={{marginTop: "10px" }}>
            <h2 className="carousel-text">{NewsArray[2].heading}</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RightCarusal;

// <div className="carusal_right">
// 	<div className="heading">
// 		<div className="title">
// 			<h1>{title}</h1>
// 		</div>
// 		<div className="navigation_icon">
// 			<div className="arrow_previous" onClick={scrollLeft}>
// 				<img src={ArrowPrevious} alt="ArrowPrevious" />
// 			</div>
// 			<div className="arrow_next" onClick={scrollRight}>
// 				<img src={ArrowNext} alt="ArrowNext" />
// 			</div>
// 		</div>
// 	</div>

// 	<div className="carusal_container" ref={scrollRef}>
// 		{ContentArray.map((item) => (
// 			<div className="carusal_card" key={item.id}>
// 				<div className="carusal_img">
// 					<img src={item.img} alt="work" />
// 				</div>
// 				<div className="carusal_tag">
// 					<span>{item.tag && item.tag}</span>
// 					<span>{item.date && item.date}</span>
// 				</div>

// 				<div className="carusal_heading">
// 					<h2>{item.heading && item.heading}</h2>
// 				</div>

// 				<div className="carusal_description">
// 					<p>{item.description && item.description}</p>
// 				</div>
// 			</div>
// 		))}
// 	</div>

// 	<button className="carusal_btn">
// 		<Link to="/dishes" className="link text-decoration-none">
// 			{buttonText} <CgArrowLongRight className="arrow" />
// 		</Link>
// 	</button>
// </div>
