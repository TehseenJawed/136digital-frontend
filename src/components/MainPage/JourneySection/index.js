import "./style.scss";
import RocketFire from "../../../assets/videos/rocket-fire.gif";
import { Link } from "react-router-dom";
import { CgArrowLongRight } from "react-icons/cg";
import JourneyImg from "../../../assets/images/mainPage/Journey/Journey.png";
import { BiRightArrowAlt } from "react-icons/bi";

const JourneySection = () => {
  return (
    <div className="journey_sectioon">
      <img src={RocketFire} alt="rocket" />

      <div className="content">
        <div className="journey-Img">{/* <img src={JourneyImg} /> */}</div>
        <div className="text-content">
          <h1>Journey</h1>
          <p>Effective subscriptions for brands for all marketing services.</p>

          <div className="work_btn_div">
            <Link style={{textDecoration:'none'}} to="/journey">
              <div className="buttonContainer">
                <div className="footer-button">
                  <div className="foolter-icon-button">
                    <BiRightArrowAlt color={"#00253f"} size={30} />
                  </div>
                  <div className="foolter-icon-card">Show More</div>
                </div>
                <div className="foolter-icon-card-absolute">Show More</div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default JourneySection;
