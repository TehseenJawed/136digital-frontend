import ArrowNext from "../../../assets/images/next-icon.svg";
import ArrowPrevious from "../../../assets/images/previous-icon.svg";
import { MdOutlineDateRange } from "react-icons/md";
import CupImg from "../../../assets/images/mainPage/News/cup.png";
import PeopleImg from "../../../assets/images/mainPage/News/people.png";
import UtilImg from "../../../assets/images/mainPage/News/ultilites.png";

import { useRef } from "react";
import { Link } from "react-router-dom";

import { CgArrowLongRight, CgHello } from "react-icons/cg";

import "./style.scss";

const NewSection = () => {
  const newsRef = useRef(null);

  const NewsArray = [
    {
      id: 0,
      img: UtilImg,
      heading: "136 DIGITAL. Company news headline",
      date: "March 10, 2021 ",
      w: "370px",
      h: "565px",
      top: "0px",
      postion: "relative",
    },
    {
      id: 1,
      img: PeopleImg,
      heading: "136 DIGITAL. Company news headline",
      date: "March 10, 2021 ",
      w: "370px",
      h: "565px",
      top: "100px",
      postion: "relative",
    },
    {
      id: 2,
      img: CupImg,
      heading: "136 DIGITAL. Company news headline",
      date: "March 10, 2021 ",
      w: "370px",
      h: "563px",
      top: "-50px",
      postion: "relative",
    },
  ];

  return (
    <div className="news_scetion">
      <div className="heading">
        <div className="title">
          <h1>News</h1>
        </div>
      </div>

      <div className="news_container">
        <div id="work_card0" className="work_card" key={NewsArray[0].id}>
          <div className="img-div">
            <img src={NewsArray[0].img} alt="work" />
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[0].date}
            </span>
          </div>

          <div style={{ fontSize: "28px", marginTop: "10px" }}>
            <h2>{NewsArray[0].heading}</h2>
          </div>
        </div>
        <div id="work_card1" className="work_card" key={NewsArray[1].id}>
          <div className="img-div">
            <img src={NewsArray[1].img} alt="work" />
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[1].date}
            </span>
          </div>

          <div style={{ fontSize: "28px", marginTop: "10px" }}>
            <h2>{NewsArray[1].heading}</h2>
          </div>
        </div>
        <div id="work_card2" className="work_card" key={NewsArray[2].id}>
          <div className="img-div">
            <img src={NewsArray[2].img} alt="work" />
          </div>
          <div className="work_card_date">
            <span>
              <MdOutlineDateRange />
              {NewsArray[2].date}
            </span>
          </div>

          <div style={{ fontSize: "28px", marginTop: "10px" }}>
            <h2>{NewsArray[2].heading}</h2>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NewSection;
