import "./style.scss";
import { useState, useEffect } from "react";

import Branding from "../../../assets/images/mainPage/capabillities/branding.png";
import Website from "../../../assets/images/mainPage/capabillities/websites.png";
import Ecommerce from "../../../assets/images/mainPage/capabillities/ecommerce.png";
import VR from "../../../assets/images/mainPage/capabillities/vr.png";
import Performance from "../../../assets/images/mainPage/capabillities/performance.png";
import Img1 from "../../../assets/images/CapabilitiesPage/capabilities-img-one.png";
import Img2 from "../../../assets/images/CapabilitiesPage/capabilities-img-two.png";
import Img3 from "../../../assets/images/CapabilitiesPage/capabilities-img-five.png";
import { Link } from "react-router-dom";
import { CgArrowLongRight } from "react-icons/cg";

const CapabillitiesSection = () => {
  const [img, setImg] = useState(Img1);
  const [para, setPara] = useState(
    "1 We help a diverse clientele build result-driven web development products for different industries."
  );
  const [detail, setDetail] = useState(
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
  );
  const [detailHeading, setDetailHeading] = useState("Branding & Identity");

  const handleClick = (e) => {
    console.log(e.target.value, "<== value");
    console.log(typeof e.target.value);
    switch (e.target.value) {
      case "2":
        setImg(Img2);
        setPara(
          "2 We help a diverse clientele build result-driven web development products for different industries."
        );
        setDetail(
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        );
        setDetailHeading("Website & Digital Platforms");
        break;
      case "1":
        setImg(Img1);
        setPara(
          "1 We help a diverse clientele build result-driven web development products for different industries."
        );
        setDetail(
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        );
        setDetailHeading("Branding & Identity");
        break;
      case "3":
        setImg(Img3);
        setPara(
          "3 We help a diverse clientele build result-driven web development products for different industries."
        );
        setDetail(
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
        );
        setDetailHeading("eCommerce Experiances");
        break;
    }
  };

  useEffect(() => console.log("mounted"), [para, detail, img]);

  return (
    <div className="capabillities_section">
      <div className="img-div">
        <img className="side-image" src={img} />
      </div>
      <div className="capa-right-sec">
        <div className="heading">
          <h1>Our capabilities</h1>
        </div>
        <div className="capa-para">
          <p>{para}</p>
        </div>
        <div className="capa-list-div">
          <ul className="capa-list">
            <button
              value={1}
              onClick={(e) => {
                handleClick(e);
              }}
              className="cool-link"
            >
              Branding & Identity
            </button>
            <button
              value={2}
              onClick={(e) => {
                handleClick(e);
              }}
              className="cool-link"
            >
              Websites & Digital Platforms
            </button>
            <button
              value={3}
              onClick={(e) => {
                handleClick(e);
              }}
              className="cool-link"
            >
              eCommerce Experiences
            </button>
          </ul>
        </div>
        <div className="capa-detail-block">
          <div className="detail-heading">{detailHeading}</div>
          <div className="detail-paragraph">{detail}</div>
        </div>
      </div>
    </div>
  );
};

export default CapabillitiesSection;
