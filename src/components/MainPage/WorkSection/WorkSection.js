import ArrowNext from "../../../assets/images/next-icon.svg";
import ArrowPrevious from "../../../assets/images/previous-icon.svg";

import HandBag from "../../../assets/images/handbag.svg";
import GameController from "../../../assets/images/game-controller.svg";
import VCR from "../../../assets/images/vcr.svg";
import FlowerPot from "../../../assets/images/flowerpot.png";
import { useRef } from "react";
import { Link } from "react-router-dom";
import { CgArrowLongRight } from "react-icons/cg";
import { BiRightArrowAlt } from "react-icons/bi";

function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

const WorkSection = () => {
  const workRef = useRef(null);

  const WorkArray = [
    {
      id: 0,
      img: VCR,
      heading: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      tag: "Performance Marketing",
      width: "570px",
      height: "565px",
    },
    {
      id: 1,
      img: HandBag,
      heading: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      tag: "Performance Marketing",
      width: "470px",
      height: "565px",
    },
    {
      id: 2,
      img: FlowerPot,
      heading: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      tag: "Performance Marketing",
      width: "378px",
      height: "599px",
    },
    {
      id: 2,
      img: GameController,
      heading: "Umbro Kaixi —Elegant Handbags for Elegant Women",
      tag: "Performance Marketing",
      width: "678px",
      height: "565px",
    },
  ];

  console.log(WorkArray);

  const scrollLeft = () => {
    if (workRef.current) {
      workRef.current.scrollBy({
        top: 0,
        left: -400,
        behavior: "smooth",
      });
    }
  };

  const scrollRight = () => {
    if (workRef.current) {
      workRef.current.scrollBy({
        top: 0,
        left: 480,
        behavior: "smooth",
      });
    }
  };

  return (
    <div className="work_section">
      {/* <div className="align-absolute">  </div> */}
      <div className="heading">
        <div className="title">
          <h1>We guide game-changing companies, across platforms & places, through agile design & digital experience.</h1>
        </div>
      </div>

      <div className="work_sectionAlpha">
        <div className="work_container" ref={workRef}>
          <div id="work_card0" className="work_card0" key={WorkArray[0].id}>
            <div className="work_img">
              <img src={WorkArray[0].img} alt="work" />
            </div>
            <div className="work_tag">
              <span>{WorkArray[0].tag}</span>
            </div>

            <div className="work_heading">
              <h2>{WorkArray[0].heading}</h2>
            </div>
          </div>
          <div id="work_card1" className="work_card1" key={WorkArray[1].id}>
            <div className="work_img">
              <img src={WorkArray[1].img} alt="work" />
            </div>
            <div className="work_tag">
              <span>{WorkArray[1].tag}</span>
            </div>

            <div className="work_heading">
              <h2>{WorkArray[1].heading}</h2>
            </div>
          </div>
          <div id="work_card2" className="work_card2" key={WorkArray[2].id}>
            <div className="work_img">
              <img src={WorkArray[2].img} alt="work" />
            </div>
            <div className="work_tag">
              <span>{WorkArray[2].tag}</span>
            </div>

            <div className="work_heading">
              <h2>{WorkArray[2].heading}</h2>
            </div>
          </div>
          <div id="work_card3" className="work_card3" key={WorkArray[3].id}>
            <div className="work_img">
              <img src={WorkArray[3].img} alt="work" />
            </div>
            <div className="work_tag">
              <span>{WorkArray[3].tag}</span>
            </div>

            <div className="work_heading">
              <h2>{WorkArray[3].heading}</h2>
            </div>
          </div>
        </div>
      </div>
      <div className="work_btn_div">
        <div className="complete-button">
          <Link style={{ textDecoration: 'none' }} to="/work">
            <div className="buttonContainer">
              <div className="footer-button">
                <div className="foolter-icon-button">
                  <BiRightArrowAlt color={"white"} size={30} />
                </div>
                <div className="foolter-icon-card">Show More</div>
              </div>
              <div className="foolter-icon-card-absolute">Show More</div>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default WorkSection;
