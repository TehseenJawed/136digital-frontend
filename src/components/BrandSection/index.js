import { useState, useRef } from "react";
import ArrowNext from "../../assets/images/next-icon.svg";
import ArrowPrevious from "../../assets/images/previous-icon.svg";
import { BsJustify, BsPlayFill } from "react-icons/bs";
import img1 from "../../assets/images/mainPage/cooperation/img1.png";
import img2 from "../../assets/images/mainPage/cooperation/img2.png";
import img3 from "../../assets/images/mainPage/cooperation/img3.png";
import {
  IoIosArrowDroprightCircle,
  IoIosArrowDropleftCircle,
} from "react-icons/io";
import leftColon from "../../assets/images/mainPage/cooperation/left.png";
import rightColon from "../../assets/images/mainPage/cooperation/right.png";
import "./style.scss";
import cooperation from "../..//assets/images/mainPage/cooperation/Cooperation.png";

import Image1 from "../../assets/images/mainPage/brand/1.svg";
import Image2 from "../../assets/images/mainPage/brand/2.svg";
import Image3 from "../../assets/images/mainPage/brand/3.svg";
import Image4 from "../../assets/images/mainPage/brand/4.svg";
import Image5 from "../../assets/images/mainPage/brand/5.svg";
import Image6 from "../../assets/images/mainPage/brand/6.svg";
import Vid1 from "../../assets/videos/main-page/vid1.mp4";

const BrandSection = ({ Image1, Image2, Image3, Image4, Image5, Image6 }) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const [videoPlay, setVideoPlay] = useState(false);
  const [indexer, setIndexer] = useState(0);
  const [indexer2, setIndexer2] = useState(2);
  const videoRef = useRef("");
  const handleClick = (event) => {
    // videoRef.play();
    setVideoPlay(true);
  };
  console.log("videoRef videoRef videoRef ", videoRef);
  const BrandContent = [
    {
      id: 0,
      img: img1,
      name: "Alice Jhonns",
      position: "Product Marketing Manager in Autumn",
      content:
        '"1We partnered with to transform our branding from what it was during our first few months as a company, to what it is today (Seed to Series A). They were able to articulate our brand requirements into a perfect vision."',
    },
    {
      id: 1,
      img: img2,
      name: "Jared Schwitzke",
      position: "Product Marketing Manager in Spring",
      content:
        '"2That partnered with to transform our branding from what it was during our first few months as a company, to what it is today (Seed to Series A). They were able to articulate our brand requirements into a perfect vision."',
    },
    {
      id: 2,
      img: img3,
      name: "Nico Sanchez",
      position: "Product Marketing Manager in Winter",
      content:
        '"3You partnered with to transform our branding from what it was during our first few months as a company, to what it is today (Seed to Series A). They were able to articulate our brand requirements into a perfect vision."',
    },
  ];

  const prevSlide = () => {
    console.log("i worked");
    setCurrentSlide(
      currentSlide === 0 ? BrandContent.length - 1 : currentSlide - 1
    );
  };

  const nextSlide = () => {
    setCurrentSlide(
      currentSlide === BrandContent.length - 1 ? 0 : currentSlide + 1
    );
  };

  console.log(currentSlide);

  const isActive = (active) => {
    switch (active) {
      case 0: {
        return { index1: 2, index2: 1 };
      }
      case 1: {
        return { index1: 0, index2: 2 };
      }
      case 2: {
        return { index1: 1, index2: 0 };
      }
    }
  };

  if (window.location.pathname == "/") {
    return (
      <div className="brand_section">
        <div className="branding_images ">
          <div className="piece">
            <img src={Image1} alt="" class="slider-images" />
          </div>
          <div className="piece">
            <img src={Image2} alt="" />
          </div>
          <div className="piece">
            <img src={Image3} alt="" />
          </div>
          <div className="piece">
            <img src={Image4} alt="" />
          </div>
          <div className="piece">
            <img src={Image5} alt="" />
          </div>
          <div className="piece">
            <img src={Image6} alt="" />
          </div>
        </div>
        <div className="heading">
          <h1>Brand Cooperation</h1>
        </div>
        <div className="slider_container">
          <IoIosArrowDropleftCircle
            // style={{ width: 30, height: 30 }}
            className="arrow-button next-icon"
            onClick={() => {
              prevSlide();
            }}
          />

          {BrandContent.map((brand, index) => (
            <div
              className={index === currentSlide ? "slide active" : "slide"}
              key={index}
            >
              {index === currentSlide && (
                <div className="content">
                  <div>
                    <img
                      onClick={() => {
                        prevSlide();
                      }}
                      style={{
                        marginRight: "20px",
                        width: "50px",
                        height: "50px",
                      }}
                      src={BrandContent[isActive(index).index1].img}
                    />
                    <img src={brand.img} />
                    <img
                      onClick={() => {
                        nextSlide();
                      }}
                      style={{
                        marginLeft: "20px",
                        width: "50px",
                        height: "50px",
                      }}
                      src={BrandContent[isActive(index).index2].img}
                    />
                  </div>
                  <p style={{ fontSize: "24px", marginTop: "20px" }}>
                    {brand.name}
                  </p>
                  <p style={{ fontSize: "16px", fontWeight: "light" }}>
                    {brand.position}
                  </p>
                  <p>{brand.content}</p>
                </div>
              )}
            </div>
          ))}

          <IoIosArrowDroprightCircle
            className="arrow-button next-icon" 
            onClick={nextSlide}
          />
        </div>
      </div>
    );
  } else {
    return (
      <div className="main-heading-container">
        <div className="heading-container1">
          <h1 className="heading2">
            This raw, early exposure to personal branding fueled the creation of
            our agency
          </h1>
        </div>
        <div className="brand_section">
          <div className="heading">
            <h1>Brand Cooperation</h1>
          </div>
          <div className="slider_container">
            <IoIosArrowDropleftCircle
              className="next-icon"
              onClick={prevSlide}
            />

            {BrandContent.map((brand, index) => (
              <div
                className={index === currentSlide ? "slide active" : "slide"}
                key={index}
              >
                {index === currentSlide && (
                  <div className="content">
                    <div className="images-div">
                      <img
                        style={{
                          marginRight: "20px",
                          width: "50px",
                          height: "50px",
                        }}
                        onClick={prevSlide}
                        src={BrandContent[isActive(index).index1].img}
                      />
                      <img src={brand.img} />
                      <img
                        onClick={prevSlide}
                        style={{
                          marginLeft: "20px",
                          width: "50px",
                          height: "50px",
                        }}
                        src={BrandContent[isActive(index).index2].img}
                      />
                    </div>
                    <p style={{ fontSize: "24px", marginTop: "20px" }}>
                      {brand.name}
                    </p>
                    <p style={{ fontSize: "16px", fontWeight: "light" }}>
                      {brand.position}
                    </p>
                    <p
                      style={{
                        color: "#848484",
                        fontSize: "18px",
                      }}
                    >
                      <leftColon />
                      {brand.content}
                      <rightColon />
                    </p>
                  </div>
                )}
              </div>
            ))}

            <IoIosArrowDroprightCircle
              onClick={nextSlide}
              className="next-icon"
            />
          </div>
        </div>
        <div className="branding_images">
          <div className="">
            <img src={Image1} alt="" />
          </div>
          <div className="">
            <img src={Image2} alt="" />
          </div>
          <div className="">
            <img src={Image3} alt="" />
          </div>
          <div className="">
            <img src={Image4} alt="" />
          </div>
          <div className="">
            <img src={Image5} alt="" />
          </div>
          <div className="">
            <img src={Image6} alt="" />
          </div>
        </div>
      </div>
    );
  }
};

export default BrandSection;
