import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";

import HeaderLogoWhite from "../assets/images/header-logo-white.svg";
import HeaderLogoBlack from "../assets/images/header-logo-black.svg";
import Cancel from "../assets/images/cancel.svg";
import {
  TiSocialFacebook,
  TiSocialTwitter,
  TiSocialInstagram,
} from "react-icons/ti";
import { SiMedium } from "react-icons/si/";
import { GiHamburgerMenu } from "react-icons/gi";
import { CgArrowLongRight } from "react-icons/cg";

const Header = () => {
  const { pathname } = useLocation();

  const [showSmallScreenMenu, setShowSmallScreenMenu] = useState(false);

  const [headerColor, setHeaderColor] = useState("black");

  useEffect(() => {
    if (pathname === "/") {
      setHeaderColor("white");
    } else {
      setHeaderColor("black");
    }
  }, [pathname]);

  useEffect(() => {
    if (showSmallScreenMenu) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [showSmallScreenMenu]);
  return (
    <div className="header">
      <div className="header_logo">
        <Link to="/">
          <img
            className="setImage"
            src={headerColor === "white" ? HeaderLogoWhite : HeaderLogoBlack}
            alt="HeaderLogo"
          />
        </Link>
      </div>

      <div className=" header_links d-none d-lg-flex ">
        <ul>
          <li className="cool-link">
            <Link
              className={`${
                headerColor === "white"
                  ? "navlinksWhite"
                  : `${
                      window.location.pathname == "/about-us"
                        ? "navlinksActive"
                        : "navlinksBlack"
                    }`
              } text-decoration-none `}
              to="/about-us"
            >
              ABOUT US
            </Link>
          </li>
          {/* <li className="cool-link">
            <Link
              className={`${
                headerColor === "white"
                  ? "navlinksWhite"
                  : `${
                      window.location.pathname == "/work"
                        ? "navlinksActive"
                        : "navlinksBlack"
                    }`
              } text-decoration-none `}
              to="/work"
            >
              WORK
            </Link>
          </li> */}
          <li className="cool-link">
            <Link
              className={`${
                headerColor === "white"
                  ? "navlinksWhite"
                  : `${
                      window.location.pathname == "/capabilities"
                        ? "navlinksActive"
                        : "navlinksBlack"
                    }`
              } text-decoration-none `}
              to="/capabilities"
            >
              CAPABILITIES
            </Link>
          </li>
          <li className="cool-link">
            <Link
              className={`${
                headerColor === "white"
                  ? "navlinksWhite"
                  : `${
                      window.location.pathname == "/journey"
                        ? "navlinksActive"
                        : "navlinksBlack"
                    }`
              } text-decoration-none `}
              to="/journey"
            >
              JOURNEY
            </Link>
          </li>
          <li className="cool-link">
            <Link
              className={`${
                headerColor === "white"
                  ? "navlinksWhite"
                  : `${
                      window.location.pathname == "/contact-us"
                        ? "navlinksActive"
                        : "navlinksBlack"
                    }`
              } text-decoration-none `}
              to="/contact-us"
            >
              CONTACT
            </Link>
          </li>
          <li className="social-link">
            <a
              className={`${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              href="https://www.facebook.com/"
              target="_blank"
              rel="noreferrer"
            >
              <TiSocialFacebook />
            </a>
          </li>
          <li className="social-link">
            <a
              className={`${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              href="https://twitter.com/"
              target="_blank"
              rel="noreferrer"
            >
              <TiSocialTwitter />
            </a>
          </li>
          <li className="social-link">
            <a
              className={`${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              href="https://www.instagram.com/"
              target="_blank"
              rel="noreferrer"
            >
              <TiSocialInstagram />
            </a>
          </li>
          <li className="social-link">
            <a
              className={`${
                headerColor === "white" ? "navlinksWhite" : "navlinksBlack"
              } text-decoration-none `}
              href="https://www.medium.com/"
              target="_blank"
              rel="noreferrer"
            >
              <SiMedium />
            </a>
          </li>
        </ul>
      </div>

      <div
        className={`${
          headerColor === "white" ? "hamburger_white" : "hamburger_black"
        } hamburger_menu d-lg-none`}
        onClick={() => setShowSmallScreenMenu(true)}
      >
        <GiHamburgerMenu />
      </div>

      {showSmallScreenMenu && (
        <div className="mobile_menu">
          <div className="mobile_menu_header">
            <div className="header_logo">
              <img src={HeaderLogoWhite} alt="headerLogo" />
            </div>

            <div
              className="cancel"
              onClick={() => setShowSmallScreenMenu(false)}
            >
              <img src={Cancel} alt="cancel" />
            </div>
          </div>

          <div className="mobile_menu_links">
            <ul>
              <li>
                <Link
                  className="text-white text-decoration-none"
                  to="/about-us"
                  onClick={() => setShowSmallScreenMenu(false)}
                >
                  About us
                </Link>
              </li>
              <li>
                <Link
                  className="text-white text-decoration-none"
                  to="/work"
                  onClick={() => setShowSmallScreenMenu(false)}
                >
                  Work
                </Link>
              </li>
              <li>
                <Link
                  className="text-white text-decoration-none"
                  to="/capabilities"
                  onClick={() => setShowSmallScreenMenu(false)}
                >
                  Capabilities
                </Link>
              </li>
              <li>
                <Link
                  className="text-white text-decoration-none"
                  to="/journey"
                  onClick={() => setShowSmallScreenMenu(false)}
                >
                  Journey
                </Link>
              </li>
              <li>
                <Link
                  className="text-white text-decoration-none"
                  to="/contact-us"
                  onClick={() => setShowSmallScreenMenu(false)}
                >
                  Contact
                </Link>
              </li>
              <div className="social-links-mobile-view">
                <li className="social-link">
                  <a
                    className={`${
                      headerColor === "white"
                        ? "navlinksWhite"
                        : "navlinksBlack"
                    } text-decoration-none `}
                    href="https://www.facebook.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <TiSocialFacebook style={{ color: "white" }} />
                  </a>
                </li>
                <li className="social-link">
                  <a
                    className={`${
                      headerColor === "white"
                        ? "navlinksWhite"
                        : "navlinksBlack"
                    } text-decoration-none `}
                    href="https://twitter.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <TiSocialTwitter style={{ color: "white" }} />
                  </a>
                </li>
                <li className="social-link">
                  <a
                    className={`${
                      headerColor === "white"
                        ? "navlinksWhite"
                        : "navlinksBlack"
                    } text-decoration-none `}
                    href="https://www.instagram.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <TiSocialInstagram style={{ color: "white" }} />
                  </a>
                </li>
                <li className="social-link">
                  <a
                    className={`${
                      headerColor === "white"
                        ? "navlinksWhite"
                        : "navlinksBlack"
                    } text-decoration-none `}
                    href="https://www.medium.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <SiMedium style={{ color: "white" }} />
                  </a>
                </li>
              </div>
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default Header;
